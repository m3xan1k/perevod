import requests

import constants
import utils
import db
from config import config


def _request_translation(
    translation_params: utils.TranslationParams
) -> requests.Response:
    request_params = {
        "client": "gtx",
        "dt": "t",
        **vars(translation_params),
    }

    response = requests.get(
        url=constants.BASE_URL,
        params=request_params,
    )
    response.raise_for_status()

    return response


def _parse_response(response: requests.Response) -> str:
    """
    Example
    [[['привет', 'hello', None, None, 10]], ...]
    """
    return response.json()[0][0][0]


def get_translation(
    translation_params: utils.TranslationParams
) -> str:

    if config["DEFAULT"]['CACHE']:
        if translated_text := db.query_translation(translation_params):
            return translated_text[0]
        else:
            response = _request_translation(translation_params)
            translated_text = str.capitalize(_parse_response(response))
            db.save_translation(translation_params, translated_text)
            return translated_text

    response = _request_translation(translation_params)
    return _parse_response(response)
