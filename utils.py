import argparse
from dataclasses import dataclass

from config import config


@dataclass
class TranslationParams:
    sl: str
    tl: str
    q: str


def _create_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s",
        "--source-language",
        help="Source language code",
        type=str,
    )
    parser.add_argument(
        "-t",
        "--target-language",
        help="Target language code",
        type=str,
    )
    parser.add_argument(
        "text",
        help="Text to translate",
        type=str,
    )
    return parser


def parse_arguments_to_params() -> TranslationParams:
    parser = _create_argparser()
    args = parser.parse_args()

    for arg, val in vars(args).items():
        if val is None:
            while not getattr(args, arg):
                setattr(
                    args,
                    arg,
                    input(f"{arg}[{config['DEFAULT'][arg]}]: ") or
                    config["DEFAULT"][arg]
                )
    return TranslationParams(
        sl=str.upper(args.source_language),
        tl=str.upper(args.target_language.upper()),
        q=str.capitalize(args.text.capitalize()),
    )


def print_result(params: TranslationParams, translated: str) -> None:
    print(f"{params.q}[{params.sl}] -> {translated}[{params.tl}]")
