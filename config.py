import configparser
from pathlib import Path
import os


BASE_DIR = Path(__file__).resolve().parent


def parse_config() -> configparser.ConfigParser:
    config = configparser.ConfigParser()
    config.read(os.path.join(BASE_DIR, "config"))
    return config


config = parse_config()
