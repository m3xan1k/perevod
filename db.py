import os
import sqlite3

import utils
from config import BASE_DIR


conn = sqlite3.Connection(os.path.join(BASE_DIR, "db.sqlite3"))


MIGRATION = """
BEGIN TRANSACTION;
CREATE TABLE languages(
    id integer primary key,
    name varchar(2) not null,
    UNIQUE(name)
);

CREATE TABLE translations(
    id integer primary key,
    source_language_id integer not null,
    target_language_id integer not null,
    source_text text not null,
    translated_text text not null,
    FOREIGN KEY(source_language_id) REFERENCES languages(id),
    FOREIGN KEY(target_language_id) REFERENCES languages(id),
    UNIQUE(
        source_language_id,
        target_language_id,
        source_text,
        translated_text
    )
);
COMMIT;
"""


def init_db():
    with conn:
        cur = conn.cursor()
        cur.execute(
            """
            select * from sqlite_master
            where type = 'table'
            """
        )
        if not cur.fetchall():
            conn.isolation_level = None
            for statement in MIGRATION.split(";"):
                cur.execute(statement)


def query_translation(params: utils.TranslationParams) -> str:
    with conn:
        cur = conn.cursor()
        cur.execute(
            """
            select t.translated_text
            from translations t
            where t.source_text = ?
                and t.source_language_id = (
                    select id from languages
                    where name = ?
                )
                and t.target_language_id = (
                    select id from languages
                    where name = ?
                )
            """,
            (params.q, params.sl, params.tl)
        )
        return cur.fetchone()


def save_translation(params: utils.TranslationParams, translated_text: str) -> None:
    with conn:
        cur = conn.cursor(
)
        for lang_name in (params.sl, params.tl):
            cur.execute("select id from languages where name = ?", (lang_name, ))
            if not cur.fetchone():
                cur.execute(
                    """
                    insert into languages(name)
                    values(?)
                    """,
                    (lang_name, )
                )
        cur.execute(
            """
            insert into translations(
                source_language_id,
                target_language_id,
                source_text,
                translated_text
            )
            values(
                (select id from languages where name = ?),
                (select id from languages where name = ?),
                ?,
                ?
            )
            """,
            (params.sl, params.tl, params.q, translated_text)
        )
