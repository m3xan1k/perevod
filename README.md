# Perevod

## 5 steps

1. Fetch code

```
git clone https://codeberg.org/m3xan1k/perevod
```

2. Set defaults in `config` file (optional)

3. Make symlink `ln -s $PWD/main.py ~/.local/bin/perevod`

4. Make it executable `chmod +x ~/.local/bin/perevod`

5. Enjoy `perevod -s en -t ru 'hello world'`