#!/usr/bin/env python3

import utils
import translate
import db
from config import config


if __name__ == '__main__':
    enabled_cache = config["DEFAULT"]["CACHE"]

    if enabled_cache:
        db.init_db()

    translation_params = utils.parse_arguments_to_params()
    translated = translate.get_translation(translation_params)
    utils.print_result(translation_params, translated)
